/* @file  monitor.h
 * @date  <date>
 * @brief <Descriptions>
 *
 * Copyright (C) 2020 NIIC EDA
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE file for details.
 */

#ifndef SRC_UTIL_MONITOR_H_
#define SRC_UTIL_MONITOR_H_
#include <sys/types.h>
#include <sys/resource.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <unordered_map>
#include <string>
#include <iomanip>

#include "util/message.h"

namespace open_edi {
namespace util {

using MonitorId = uint32_t;
using ResourceTypes = uint32_t;

const uint32_t kMaxNumMonitorId = UINT_MAX - 1;
const uint32_t kInvalidMonitorId = 0;

enum ResourceType : uint32_t {
    kElapsedTime        = 0x00000001,    // current time - start time
    kCpuTime            = 0x00000002,
    kUserCpuTime        = 0x00000004,
    kSysCpuTime         = 0x00000008,
    kPhysicalMem        = 0x00000010,    // VmRSS in /proc/<pid>/status
    kPhysicalPeak       = 0x00000020,
    kVirtualMem         = 0x00000040,    // VmSize in /proc/<pid>/status
    kVirtualPeak        = 0x00000080,
    kUnkown
};

class MonitorInformation {
  public:
    MonitorInformation();
    ~MonitorInformation() {}

    void recordElapsedTime(struct timeval start_time);
    double getElapsedTime() { return elapsed_time_; }
    void resumeTime(struct timeval start_time, struct rusage start_usage);

    void setCpuTime(struct rusage start_usage);
    double getCpuTime() { return cpu_time_; }
    double getUserCpuTime() { return user_cpu_time_; }
    double getSysCpuTime() { return sys_cpu_time_; }

    void setPeakMemory();
    std::string getVirtualMem(char unit[]);
    std::string getPhysicalMem(char unit[]);
    std::string getVirtualPeak(char unit[]);
    std::string getPhysicalPeak(char unit[]);

  private:
    double elapsed_time_;
    double cpu_time_;
    double user_cpu_time_;
    double sys_cpu_time_;

    double p_elapsed_time_;         // record paused time
    double p_user_cpu_time_;
    double p_sys_cpu_time_;

    uint64_t physical_mem_;
    uint64_t physical_mem_peak_;
    uint64_t virtual_mem_;
    uint64_t virtual_mem_peak_;

    std::string memUnitConversion(char unit[], uint64_t mem);
};

class Monitor {
  public:
    Monitor();

    struct timeval getStartTime() { return start_time_; }
    void recordCurrentInfo();
    void resumeCurrentInfo();

    MonitorInformation getStartInfo() { return start_info_; }
    MonitorInformation & getCurrentInfo();

    void pause();
    void reset();
    void resume();

    enum MonitorState {
        kMonitorRunning,
        kMonitorPaused,
        kUnknown
    };

  private:
    MonitorState state_;
    struct timeval start_time_;
    struct rusage start_usage_;
    MonitorInformation start_info_;
    MonitorInformation current_info_;
};

static MonitorInformation peak_memory_calculate;

class MonitorManager {
  public:
    MonitorManager();
    ~MonitorManager();
    MonitorId createMonitor();
    Monitor* queryMonitor(MonitorId monitor_id) {
        return monitor_map_[monitor_id];
    }
    bool outputProcessBar(MonitorId monitor_id, ResourceTypes resource_types,
          const char* description);
    bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
          const char* description);
    bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
          FILE *fp, const char* description);
    bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
          std::ofstream &fp, const char* description);
    bool pauseMonitor(MonitorId monitor_id);
    bool resumeMonitor(MonitorId monitor_id);
    bool resetMonitor(MonitorId monitor_id);
    bool destroyMonitor(MonitorId monitor_id);
    static void calculateMemory(int signo);
    static void periodTimerGetPeak();
    void checkResourceTypes(std::string &type_str, ResourceTypes resource_types,
          MonitorInformation current_info);

  private:
    MonitorId id_;
    MonitorId unused_num_id_;
    std::unordered_map<MonitorId, Monitor*> monitor_map_;
    char vir_mem_unit_[6];
    char phy_mem_unit_[6];
};

extern MonitorId createMonitor();
extern Monitor* getMonitor(MonitorId monitor_id);
extern Monitor* queryMonitor(MonitorId monitor_id);
extern bool outputProcessBar(MonitorId monitor_id, ResourceTypes resource_types,
          const char* description);
extern bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
          const char* description);
extern bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
          FILE *fp, const char* description);
extern bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
          std::ofstream &ofs, const char* description);
extern bool pauseMonitor(MonitorId monitor_id);
extern bool resumeMonitor(MonitorId monitor_id);
extern bool resetMonitor(MonitorId monitor_id);
extern bool destroyMonitor(MonitorId monitor_id);

}  // namespace util
}  // namespace open_edi

#endif  // SRC_UTIL_MONITOR_H_
