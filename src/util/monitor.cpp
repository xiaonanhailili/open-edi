/* @file  monitor.cpp
 * @date  <date>
 * @brief <Descriptions>
 *
 * Copyright (C) 2020 NIIC EDA
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE file for details.
 */

#include "util/monitor.h"

namespace open_edi {
namespace util {

/**
 * @brief Global variables for non-member function.
 * 
 */
MonitorManager kMonitorManager;

/**
 * @brief Construct a new Monitor:: Monitor object.
 *        get start time and set monitor state.
 */
Monitor::Monitor() {
    if ( gettimeofday(&start_time_, NULL) < 0 ) {
        message->issueMsg(kError, "Monitor: gettimeofday() return code %d\n",
                            errno);
    }
    if ( 0 != getrusage(RUSAGE_SELF, &start_usage_) ) {
        message->issueMsg(kError, "Monitor: getrusage() return code %d\n",
                            errno);
    }

    state_ = kMonitorRunning;
}

/**
 * @brief Construct a new Monitor Information:: Monitor Information object.
 *        init member variables.
 */
MonitorInformation::MonitorInformation() {
      physical_mem_peak_ = 0;
      virtual_mem_peak_ = 0;
      p_elapsed_time_ = 0;
      p_user_cpu_time_ = 0;
      p_sys_cpu_time_ = 0;
}

/**
 * @brief calculate cpu time, including user time and system time.
 * 
 * @param start_usage 
 */
void MonitorInformation::setCpuTime(struct rusage start_usage) {
    struct rusage current_usage;
    if ( 0 != getrusage(RUSAGE_SELF, &current_usage) ) {
        message->issueMsg(kError, "setCpuTime: getrusage()"
                                    " return code %d\n", errno);
    }

    user_cpu_time_ = current_usage.ru_utime.tv_sec
            + static_cast<double>(current_usage.ru_utime.tv_usec) / 1000000.0
            - start_usage.ru_utime.tv_sec
            - static_cast<double>(start_usage.ru_utime.tv_usec) / 1000000.0
            - p_user_cpu_time_;
    sys_cpu_time_ = current_usage.ru_stime.tv_sec
            + static_cast<double>(current_usage.ru_stime.tv_usec) / 1000000.0
            - start_usage.ru_stime.tv_sec
            - static_cast<double>(start_usage.ru_stime.tv_usec) / 1000000.0
            - p_sys_cpu_time_;
    cpu_time_ = user_cpu_time_ + sys_cpu_time_;
}

/**
 * @brief get virtural memory and physical memory from /proc/<pid>/status.
 *        get peak memory from them.
 * 
 */
void MonitorInformation::setPeakMemory() {
    pid_t pid = getpid();
    char file_name[64] = {0};

    char line_buff[256] = {0};
    char name[32] = {0};
    uint64_t vmsize = 0;
    uint64_t vmrss = 0;
    char file_unit[32] = {0};

    FILE *fp = NULL;
    snprintf(file_name, sizeof(file_name), "/proc/%d/status", pid);
    fp = fopen(file_name, "r");
    if (!fp) {
        message->issueMsg(kError, "setPeakMemory: open file error"
                                    " return code %d\n", errno);
        exit(1);
    }
    while (NULL != fgets(line_buff, sizeof(line_buff), fp)) {
        sscanf(line_buff, "%s", name);
        if (0 == strncmp(name, "VmSize", 6)) {
            sscanf(line_buff, "%s %d %s", name, &vmsize, file_unit);
            virtual_mem_ = vmsize;
            continue;
        } else if (0 == strncmp(name, "VmRSS", 5)) {
            sscanf(line_buff, "%s %d %s", name, &vmrss, file_unit);
            physical_mem_ = vmrss;
            break;
        }
    }
    fclose(fp);

    virtual_mem_peak_ =
        virtual_mem_peak_ >= virtual_mem_
            ? virtual_mem_peak_ : virtual_mem_;
    physical_mem_peak_ =
        physical_mem_peak_ >= physical_mem_
            ? physical_mem_peak_ : physical_mem_;
}

std::string MonitorInformation::memUnitConversion(char unit[], uint64_t mem) {
    double mem_size = static_cast<double>(mem);
    std::string mem_str = std::to_string(mem);
    size_t length = mem_str.length();

    if (length > 3 && length <= 6) {
        mem_size /= 1024.0;
        strncpy(unit, "MiB", 3);
    } else if (length > 6 && length <= 9) {
        mem_size /= (1024.0 * 1024.0);
        strncpy(unit, "GiB", 3);
    } else if (length > 9 && length <= 12) {
        mem_size /= (1024.0 * 1024.0 * 1024.0);
        strncpy(unit, "TiB", 3);
    }

    std::stringstream ss;
    ss << std::fixed << std::setprecision(2) << mem_size;
    ss >> mem_str;

    return mem_str;
}

/**
 * @brief return virtural memory and set its unit.
 * 
 * @param unit 
 * @return double 
 */
std::string MonitorInformation::getVirtualMem(char unit[]) {
    return memUnitConversion(unit, virtual_mem_);
}

/**
 * @brief return physical memory and set its unit.
 * 
 * @param unit 
 * @return double 
 */
std::string MonitorInformation::getPhysicalMem(char unit[]) {
    return memUnitConversion(unit, physical_mem_);
}

/**
 * @brief return peak virtural memory and set its unit.
 * 
 * @param unit 
 * @return double 
 */
std::string MonitorInformation::getVirtualPeak(char unit[]) {
    return memUnitConversion(unit, virtual_mem_peak_);
}

/**
 * @brief return peak physical memory and set its unit.
 * 
 * @param unit 
 * @return double 
 */
std::string MonitorInformation::getPhysicalPeak(char unit[]) {
    return memUnitConversion(unit, physical_mem_peak_);
}

/**
 * @brief record current information, including elapsed time, cpu time and memory size.
 * 
 */
void Monitor::recordCurrentInfo() {
    current_info_.recordElapsedTime(start_time_);
    current_info_.setCpuTime(start_usage_);
    current_info_.setPeakMemory();
}

/**
 * @brief set resume time.
 * 
 */
void Monitor::resumeCurrentInfo() {
    current_info_.resumeTime(start_time_, start_usage_);
}

/**
 * @brief calculate elapsed time,
 *        elapsed_time = current_time - start_time - p_elapsed_time,
 *        p_elapsed_time is the time spend on pause.
 * 
 * @param start_time 
 */
void MonitorInformation::recordElapsedTime(struct timeval start_time) {
    struct timeval current_time;
    if ( gettimeofday(&current_time, NULL) < 0 ) {
        message->issueMsg(kError, "recordElapsedTime: gettimeofday()"
                                    " return code %d\n", errno);
    }
    elapsed_time_ = (current_time.tv_sec - start_time.tv_sec)
        + static_cast<double>(current_time.tv_usec - start_time.tv_usec)
        / 1000000.0 - p_elapsed_time_;
}

/**
 * @brief get current information, including time and memory.
 * 
 * @return MonitorInformation& 
 */
MonitorInformation & Monitor::getCurrentInfo() {
    if (state_ == kMonitorRunning) {
        recordCurrentInfo();
    }
    return current_info_;
}

/**
 * @brief set time spend on pause.
 * 
 */
void MonitorInformation::resumeTime(struct timeval start_time,
                                    struct rusage start_usage) {
    struct timeval current_time;
    if ( gettimeofday(&current_time, NULL) < 0 ) {
        message->issueMsg(kError, "resumeTime: gettimeofday()"
                                    " return code %d\n", errno);
    }
    p_elapsed_time_ = (current_time.tv_sec - start_time.tv_sec)
                    + static_cast<double>(current_time.tv_usec
                    - start_time.tv_usec) / 1000000.0
                    - elapsed_time_;
    struct rusage current_usage;
    if ( 0 != getrusage(RUSAGE_SELF, &current_usage) ) {
        message->issueMsg(kError, "resumeTime: getrusage()"
                                    " return code %d\n", errno);
    }
    p_user_cpu_time_ = current_usage.ru_utime.tv_sec
            + static_cast<double>(current_usage.ru_utime.tv_usec) / 1000000.0
            - start_usage.ru_utime.tv_sec
            - static_cast<double>(start_usage.ru_utime.tv_usec) / 1000000.0
            - user_cpu_time_;
    p_sys_cpu_time_ = current_usage.ru_stime.tv_sec
            + static_cast<double>(current_usage.ru_stime.tv_usec) / 1000000.0
            - start_usage.ru_stime.tv_sec
            - static_cast<double>(start_usage.ru_stime.tv_usec) / 1000000.0
            - sys_cpu_time_;
}

/**
 * @brief pause monitor.
 * 
 */
void Monitor::pause() {
    state_ = kMonitorPaused;
    recordCurrentInfo();
}

/**
 * @brief resume monitor.
 * 
 */
void Monitor::resume() {
    state_ = kMonitorRunning;
    resumeCurrentInfo();
}

/**
 * @brief reset monitor.
 * 
 */
void Monitor::reset() {
    if ( gettimeofday(&start_time_, NULL) < 0 ) {
        message->issueMsg(kError, "reset: gettimeofday() return code %d\n",
                            errno);
    }
    if ( 0 != getrusage(RUSAGE_SELF, &start_usage_) )
        message->issueMsg(kError, "reset: getrusage() return code %d\n",
                            errno);
    state_ = kMonitorRunning;
}

/**
 * @brief Construct a new Monitor Manager:: Monitor Manager object
 * 
 */
MonitorManager::MonitorManager() {
    id_ = kInvalidMonitorId + 1;
    unused_num_id_ = kMaxNumMonitorId;

    strncpy(vir_mem_unit_, "KiB", 3);
    strncpy(phy_mem_unit_, "KiB", 3);

    //  timer pthread
    periodTimerGetPeak();
}

/**
 * @brief Destroy the Monitor Manager:: Monitor Manager object
 * 
 */
MonitorManager::~MonitorManager() {
    std::unordered_map<MonitorId, Monitor*>::iterator it = monitor_map_.begin();
    while (it !=  monitor_map_.end()) {
        delete it->second;
        ++it;
    }
    struct itimerval value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_usec = 0;
    value.it_interval = value.it_value;
    setitimer(ITIMER_REAL, &value, NULL);
}

/**
 * @brief thread for calculating peak memory.
 * 
 * @param signo 
 */
void MonitorManager::calculateMemory(int signo) {
    peak_memory_calculate.setPeakMemory();
}

/**
 * @brief linux timer.
 * 
 */
void MonitorManager::periodTimerGetPeak() {
    if (SIG_ERR == signal(SIGALRM, calculateMemory)) {
        message->issueMsg(kError, "periodTimerGetPeak: signal()"
                                    " return code %d\n", errno);
        exit(1);
    }

    struct itimerval tv;
    tv.it_value.tv_sec = 3;
    tv.it_value.tv_usec = 0;
    tv.it_interval.tv_sec = 3;
    tv.it_interval.tv_usec = 0;

    if (0 != setitimer(ITIMER_REAL, &tv, NULL)) {
        message->issueMsg(kError, "periodTimerGetPeak: setitimer()"
                                    " return code %d\n", errno);
        exit(1);
    }
}

/**
 * @brief create new monitor.
 * 
 * @return MonitorId 
 */
MonitorId MonitorManager::createMonitor() {
    if (0 == unused_num_id_) {
        message->issueMsg(kError, "All monitor ids are used.\n");
        return kInvalidMonitorId;
    }
    MonitorId temp_id = id_;
    while (monitor_map_.find(temp_id) != monitor_map_.end()) {
        ++temp_id;
        if (temp_id == id_) {
            message->issueMsg(kError, "All monitor ids are used.\n");
            return kInvalidMonitorId;
        }
    }
    Monitor* monitor = new Monitor;
    monitor_map_[id_] = monitor;
    unused_num_id_--;

    return id_++;
}

/**
 * @brief pause monitor.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::pauseMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        monitor->pause();
        monitor->recordCurrentInfo();

        return true;
    }

    return false;
}

/**
 * @brief check resource types and then append to type str.
 * 
 * @param str 
 * @param resource_types 
 */
void MonitorManager::checkResourceTypes(std::string &type_str,
                                ResourceTypes resource_types,
                                MonitorInformation current_info) {
    if (resource_types & kElapsedTime) {
        type_str += " ElapsedTime:"
            + std::to_string(current_info.getElapsedTime())
            + "s";
    }
    if (resource_types & kCpuTime) {
        type_str += " CpuTime:"
            + std::to_string(current_info.getCpuTime())
            + "s";
    }
    if (resource_types & kUserCpuTime) {
        type_str += " UserCpuTime:"
            + std::to_string(current_info.getUserCpuTime())
            + "s";
    }
    if (resource_types & kSysCpuTime) {
        type_str += " SysCpuTime:"
            + std::to_string(current_info.getSysCpuTime())
            + "s";
    }
    if (resource_types & kPhysicalMem) {
        type_str += " PhysicalMem:"
            + current_info.getPhysicalMem(phy_mem_unit_)
            + phy_mem_unit_;
    }
    if (resource_types & kPhysicalPeak) {
        type_str += " PhysicalPeak:"
            + peak_memory_calculate.getPhysicalPeak(phy_mem_unit_)
            + phy_mem_unit_;
    }
    if (resource_types & kVirtualMem) {
        type_str += " VirtualMem:"
            + current_info.getVirtualMem(vir_mem_unit_)
            + vir_mem_unit_;
    }
    if (resource_types & kVirtualPeak) {
        type_str += " VirtualPeak:"
            + peak_memory_calculate.getVirtualPeak(vir_mem_unit_)
            + vir_mem_unit_;
    }
}

/**
 * @brief output information in one line.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::outputProcessBar(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        message->info("%s%s", type_str.c_str(), "\r");

        fflush(stdout);
        return true;
    }
    message->issueMsg(kError, "ouputProcessBar: cannot find monitor by id %d\n",
                                               monitor_id);
    return false;
}

/**
 * @brief output information per line.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::outputMonitor(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        message->info("%s%s", type_str.c_str(), "\n");

        fflush(stdout);
        return true;
    }
    message->issueMsg(kError, "ouputMonitor: cannot find monitor by id %d\n",
                                               monitor_id);
    return false;
}

/**
 * @brief write output information to file openned by FILE *.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param fp 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::outputMonitor(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   FILE *fp,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        fprintf(fp, "%s\n", type_str.c_str());

        return true;
    }
    message->issueMsg(kError, "ouputMonitor<FILE *>: cannot find"
                                " monitor by id %d\n", monitor_id);
    return false;
}

/**
 * @brief write output information to file openned by file stream.
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param fp 
 * @param description 
 * @return true 
 * @return false 
 */
bool MonitorManager::outputMonitor(MonitorId monitor_id,
                                   ResourceTypes resource_types,
                                   std::ofstream &fp,
                                   const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }

    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        Monitor *monitor = it->second;
        MonitorInformation current_info = monitor->getCurrentInfo();

        std::string type_str = description;
        checkResourceTypes(type_str, resource_types, current_info);

        fp << type_str.c_str() << '\n';

        return true;
    }
    message->issueMsg(kError, "ouputMonitor<file stream>: cannot"
                                " find monitor by id %d\n", monitor_id);
    return false;
}

/**
 * @brief reset monitor.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::resetMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        it->second->reset();
        return true;
    }
    return false;
}

/**
 * @brief resume monitor.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::resumeMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        it->second->resume();
        return true;
    }
    return false;
}

/**
 * @brief destory monitor.
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool MonitorManager::destroyMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    auto it = monitor_map_.find(monitor_id);
    if (it != monitor_map_.end()) {
        monitor_map_.erase(it);
        return true;
    }

    return false;
}

/**
 * @brief Create a Monitor object
 * 
 * @return MonitorId 
 */
MonitorId createMonitor() {
    return kMonitorManager.createMonitor();
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return Monitor* 
 */
Monitor* queryMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return nullptr;
    }
    return kMonitorManager.queryMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool outputProcessBar(MonitorId monitor_id, ResourceTypes resource_types,
          const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.outputProcessBar(monitor_id, resource_types,
                                         description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param description 
 * @return true 
 * @return false 
 */
bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
        const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.outputMonitor(monitor_id, resource_types,
                                         description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param fp 
 * @param description 
 * @return true 
 * @return false 
 */
bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
        FILE *fp, const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.outputMonitor(monitor_id, resource_types, fp,
                                         description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @param resource_types 
 * @param ofs 
 * @param description 
 * @return true 
 * @return false 
 */
bool outputMonitor(MonitorId monitor_id, ResourceTypes resource_types,
        std::ofstream &ofs, const char* description) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.outputMonitor(monitor_id, resource_types,
                                         ofs, description);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool pauseMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.pauseMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool resumeMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.resumeMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool resetMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.resetMonitor(monitor_id);
}

/**
 * @brief 
 * 
 * @param monitor_id 
 * @return true 
 * @return false 
 */
bool destroyMonitor(MonitorId monitor_id) {
    if (kInvalidMonitorId == monitor_id) {
        return false;
    }
    return kMonitorManager.destroyMonitor(monitor_id);
}

}  // namespace util
}  // namespace open_edi
