#ifndef LAYOUT_GRAPHICS_VIEW_H
#define LAYOUT_GRAPHICS_VIEW_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainterPath>
#include <QWheelEvent>
#include <qmath.h>
#include "../widget/component/components_listener.h"
#include "../widget/layer/layer_listener.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "db/util/vector_object_var.h"
#include "graphics_scene.h"
#include "items/li_die_area.h"
#include "items/li_instances.h"
#include "items/li_manager.h"
#include "items/li_nets.h"
#include "util/util.h"

namespace open_edi {
namespace gui {

#define LAYOUT_GRAPHICS_VIEW (GraphicsView::getInstance())

class GraphicsView : public QGraphicsView,
                     public LayerListener,
                     public ComponentListener {
    Q_OBJECT
  public:
    ~GraphicsView();

    LI_DieArea*   li_die_area;
    LI_Instances* li_instances;
    LI_Pins*      li_pins;
    LI_Manager*   li_manager{LI_MANAGER};
    void          readData();

    virtual void setLayerVisible(QString name, bool v) override;
    virtual void setLayerSelectable(QString name, bool v) override;
    virtual void setComponentVisible(QString name, bool v) override;
    virtual void setComponentSelectable(QString name, bool v) override;

    static GraphicsView* getInstance() {
        if (!inst_) {
            inst_ = new GraphicsView;
        }
        return inst_;
    }

  signals:
    void sendPos(int x, int y);

  public slots:
    void slotZoomIn(bool);
    void slotZoomOut(bool);
    void slotReadData();
    void setPinsVisible(bool);

  protected:
    virtual void wheelEvent(QWheelEvent* event) override;
    virtual void mouseMoveEvent(QMouseEvent* event) override;

  private:
    void                 zoomBy_(qreal factor);
    int                  die_area_w_;
    int                  die_area_h_;
    int                  view_width_;
    int                  view_height_;
    int                  scale_factor_{1};
    GraphicsScene*       scene_;
    static GraphicsView* inst_;

    GraphicsView(QWidget* parent = nullptr);

    void __zoom(qreal value);
};
} // namespace gui
} // namespace open_edi
#endif