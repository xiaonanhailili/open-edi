#ifndef EDI_GUI_LI_WIRES_H_
#define EDI_GUI_LI_WIRES_H_

#include <QPainter>
#include <qmath.h>
#include "../graphicitems/lgi_wires.h"
#include "../graphics_scene.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "db/util/vector_object_var.h"
#include "li_base.h"
#include "util/util.h"

namespace open_edi {
namespace gui {
class LI_Wires : public LI_Base {
  public:
    explicit LI_Wires(int* scale_factor);
    LI_Wires(const LI_Wires& other) = delete;
    LI_Wires& operator=(const LI_Wires& rhs) = delete;
    ~LI_Wires();

    virtual void preDraw() override;
    LGI_Wires*   getGraphicItem();
    void         drawWires(open_edi::db::Wire& wire);
    void         fillImage();

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_Wires* item_;
    QPen       pen_;
    QBrush     brush_;
};
} // namespace gui
} // namespace open_edi

#endif