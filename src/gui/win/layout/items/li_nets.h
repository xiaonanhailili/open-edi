#ifndef EDI_GUI_LI_NETS_H_
#define EDI_GUI_LI_NETS_H_

#include <QPainter>
#include <qmath.h>
#include "../graphicitems/lgi_instances.h"
#include "../graphicitems/lgi_nets.h"
#include "../graphics_scene.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "db/util/vector_object_var.h"
#include "li_base.h"
#include "li_wires.h"
#include "util/util.h"

namespace open_edi {
namespace gui {
class LI_Nets : public LI_Base {
  public:
    explicit LI_Nets(int* scale_factor);
    LI_Nets(const LI_Nets& other) = delete;
    LI_Nets& operator=(const LI_Nets& rhs) = delete;
    ~LI_Nets();

    virtual void preDraw() override;
    LGI_Nets*    getGraphicItem();
    virtual bool hasSubLI() override;
    virtual bool isMainLI() override;
    LI_Wires*    li_wires;

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_Nets* item_;
    QPen      pen_;
};
} // namespace gui
} // namespace open_edi

#endif