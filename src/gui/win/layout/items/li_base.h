#ifndef LI_BASE_H
#define LI_BASE_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QPainterPath>
#include <QPen>
#include <QPixmap>

#include "li_manager.h"

#define NO_DRAW   0
#define IMG_MODE  1
#define PATH_MODE 2
#define DRAW_MODE IMG_MODE

namespace open_edi {
namespace gui {

class LI_Base {
  public:
    LI_Base(const LI_Base& other) = delete;
    LI_Base& operator=(const LI_Base& rhs) = delete;

    explicit LI_Base(int* scale_factor) { this->scale_factor_ = scale_factor; };
    ~LI_Base(){};

    virtual void           preDraw() = 0;
    virtual void           refreshBoundSize();
    virtual void           setVisible(bool visible);
    virtual QGraphicsItem* getGraphicItem() = 0;
    virtual void           draw(QPainter* painter);
    virtual bool           hasSubLI() { return false; };
    virtual void           update();
    virtual bool           isMainLI() { return false; };
    virtual QString        getName();
    virtual bool           isVisible(){return visible_;};

    enum ObjType {
        kDieArea,
        kInstance,
        kPin,
        kNet,
        kWire
    };
    ObjType type;

  protected:
    int          bound_width_{1};
    int          bound_height_{1};
    QPixmap*     img_{nullptr};
    bool         visible_{false};
    int*         scale_factor_{nullptr};
    QPainterPath painter_path_;
    QString      name_{""};
    bool         main_li{false};
    LI_Manager*  li_mgr_{LI_MANAGER};
};

} // namespace gui
} // namespace open_edi

#endif