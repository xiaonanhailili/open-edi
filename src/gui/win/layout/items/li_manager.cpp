#include "li_manager.h"
#include "li_base.h"
namespace open_edi {
namespace gui {

LI_Manager::LI_Manager() {
}

void LI_Manager::preDrawAllItems() {
    for (auto item : li_vec_) {
        if (item->isMainLI()) {
            item->preDraw();
        }
    }
}

std::vector<LI_Base*> LI_Manager::getLiList() {
    return li_vec_;
}

void LI_Manager::addLI(LI_Base* li) {
    li_vec_.push_back(li);
    li_map_[li->getName()] = li;
}

void LI_Manager::setLiVisibleByName(QString name, bool v) {

    if (li_map_.find(name) != li_map_.end()) {
        auto li = li_map_[name];
        li->setVisible(v);
    }
}

LI_Manager* LI_Manager::inst_ = nullptr;
} // namespace gui
} // namespace open_edi