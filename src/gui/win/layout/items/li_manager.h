#ifndef LI_MANAGER_H
#define LI_MANAGER_H

#include <QMap>
#include <QString>
#include <vector>

namespace open_edi {
namespace gui {

#define LI_MANAGER (LI_Manager::getInstance())

class LI_Base;

class LI_Manager {
  public:
    ~LI_Manager() = delete;

    void                  preDrawAllItems();
    std::vector<LI_Base*> getLiList();

    static LI_Manager* getInstance() {
        if (!inst_) {
            inst_ = new LI_Manager;
        }
        return inst_;
    }

    void addLI(LI_Base* li);

    void setLiVisibleByName(QString name, bool v);

  private:
    std::vector<LI_Base*> li_vec_;
    LI_Manager();
    static LI_Manager* inst_;

    // template <typename LI>
    // void createLI(){
    //   LI li = new LI(scale_factor_);
    //   addLI(li);
    // };

    QMap<QString, LI_Base*> li_map_;
    int*                    scale_factor_;
};
} // namespace gui
} // namespace open_edi

#endif