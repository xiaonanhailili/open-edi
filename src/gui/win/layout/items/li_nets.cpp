#include "li_nets.h"

namespace open_edi {
namespace gui {

LI_Nets::LI_Nets(int* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_Nets;
    item_->setLiBase(this);
    pen_.setColor(QColor("#ff0000"));
    type     = kNet;
    li_wires = new LI_Wires(scale_factor);
    name_    = "Net";
    li_mgr_->addLI(this);
}

LI_Nets::~LI_Nets() {
}

LGI_Nets* LI_Nets::getGraphicItem() {
    return item_;
}

bool LI_Nets::hasSubLI() {
    return true;
}

bool LI_Nets::isMainLI() {
    return true;
}

void LI_Nets::draw(QPainter* painter) {
    painter->setPen(pen_);
    LI_Base::draw(painter);
}

void LI_Nets::preDraw() {
    if (!visible_) {
        return;
    }

    refreshBoundSize();
    img_->fill(Qt::transparent);

    li_wires->refreshBoundSize();
    li_wires->fillImage();

    QPainter painter(img_);
    painter.setPen(pen_);
    painter.setWindow(0,
                      bound_height_,
                      bound_width_ + VIEW_SPACE,
                      -bound_height_ - VIEW_SPACE);

    auto     tc       = open_edi::db::getTopCell();
    uint64_t num_nets = tc->getNumOfNets();
    printf("Nets %d ;\n", num_nets);
    // auto nets       = tc->getNets();
    // auto net_vector = open_edi::db::Object::addr<open_edi::db::ArrayObject<open_edi::db::ObjectId>>(nets);

    auto factor = *scale_factor_;

    auto arr_ptr = tc->getNetArray();
    if (arr_ptr) {
        for (auto iter = arr_ptr->begin(); iter != arr_ptr->end(); iter++) {
            auto net = open_edi::db::Object::addr<open_edi::db::Net>(*iter);
            if (!net) continue;
            printf("net in\n");
            // counting wire
            auto wire_array = net->getWireArray();
            if (wire_array) {
                printf("wire_array in\n");
                for (auto wire_iter = wire_array->begin(); wire_iter != wire_array->end(); ++wire_iter) {
                    auto wire = open_edi::db::Object::addr<open_edi::db::Wire>(*wire_iter);
                    li_wires->drawWires(*wire);
                }
            }

            // printf("net in\n");
        }
    }

    item_->setMap(img_);
    item_->setItemSize(bound_width_, bound_height_);

    li_wires->getGraphicItem()->setMap(img_);
    li_wires->getGraphicItem()->setItemSize(bound_width_, bound_height_);
}

} // namespace gui
} // namespace open_edi