#include "li_wires.h"

namespace open_edi {
namespace gui {
LI_Wires::LI_Wires(int* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_Wires;
    item_->setLiBase(this);
    pen_.setColor(QColor(0xff, 0, 0, 0xff));
    brush_ = QBrush(QColor(0xff, 0, 0, 0xff), Qt::Dense5Pattern);
    type   = kWire;
    name_  = "Wire";
    li_mgr_->addLI(this);
}

LI_Wires::~LI_Wires() {
}

LGI_Wires* LI_Wires::getGraphicItem() {
    return item_;
}

void LI_Wires::draw(QPainter* painter) {
    painter->setPen(pen_);
    painter->setBrush(brush_);
    LI_Base::draw(painter);
}

void LI_Wires::drawWires(open_edi::db::Wire& wire) {
    if (!visible_) {
        return;
    }
    QPainter painter(img_);

    painter.setWindow(0,
                      bound_height_,
                      bound_width_ + VIEW_SPACE,
                      -bound_height_ - VIEW_SPACE);

    painter.setPen(pen_);

    auto factor = *scale_factor_;
    auto llx    = (wire.getX()) / factor;
    auto lly    = (wire.getY()) / factor;

    auto box_width  = wire.getBBox().getWidth() / factor;
    auto box_height = wire.getBBox().getHeight() / factor;

    painter.drawRect(llx, lly, box_width, box_height);
}

void LI_Wires::fillImage() {
    img_->fill(Qt::transparent);
}

void LI_Wires::preDraw() {
    if (!visible_) {
        return;
    }
    refreshBoundSize();

    img_->fill(Qt::transparent);

    item_->setMap(img_);
    item_->setItemSize(bound_width_, bound_height_);
}

} // namespace gui
} // namespace open_edi