#ifndef EDI_GUI_COMPONENTS_PALETTE_H_
#define EDI_GUI_COMPONENTS_PALETTE_H_

#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QHBoxLayout>

namespace open_edi {
namespace gui {

class ComponentsPalette : public QWidget {
    Q_OBJECT
  public:
    ComponentsPalette(QWidget* parent = nullptr);
    ~ComponentsPalette();

  private:
    QLabel* color_label_;
    QLabel* pattern_label_;
};

} // namespace gui
} // namespace open_edi

#endif