#include "components_widget.h"

namespace open_edi {
namespace gui {

ComponentsWidget::ComponentsWidget(QWidget* parent) : QTreeWidget(parent) {

    setColumnCount(kSelectable + 1);

    connect(this, &ComponentsWidget::itemClicked, this, &ComponentsWidget::slotItemClicked);

    auto treewidgetitem = headerItem();
    treewidgetitem->setText(kName, "Name");
    treewidgetitem->setText(kColor, "");
    treewidgetitem->setText(kVisible, "V");
    treewidgetitem->setText(kSelectable, "S");

    // setColumnWidth(kName, 50);
    header()->setSectionResizeMode(kName, QHeaderView::Stretch);
    setColumnWidth(kColor, 20);
    header()->setSectionResizeMode(kColor, QHeaderView::Fixed);
    setColumnWidth(kVisible, 20);
    header()->setSectionResizeMode(kVisible, QHeaderView::Fixed);
    setColumnWidth(kSelectable, 20);
    header()->setSectionResizeMode(kSelectable, QHeaderView::Fixed);
    header()->setStretchLastSection(false);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //Instance
    auto instance_item = createTopItem(this, "Instance");

    createSubItems(createSubItem(instance_item, "Type"),
                   {"Block",
                    "StdCell",
                    "Cover",
                    "Physical",
                    "IO",
                    "Area IO",
                    "Black Box"});

    createSubItems(createSubItem(instance_item, "Function"),
                   {"Sequential",
                    "PowerSW",
                    "Isolation",
                    "Shifter",
                    "Other"});

    createSubItems(createSubItem(instance_item, "Status"),
                   {"Placed",
                    "Fixed",
                    "Cover",
                    "SoftFixed",
                    "Unplaced"});

    //Route
    createSubItems(createTopItem(this, "Route"),
                   {"Shield",
                    "Early Global",
                    "Metal Fill",
                    "Wire",
                    "Via",
                    "Patch Wire",
                    "Trim Metal"});

    //Cell
    auto cell_item = createTopItem(
      this,
      "Cell");

    createSubItems(createSubItem(cell_item, "Pin shapes"),
                   {"Block",
                    "StdCell",
                    "IO Cell",
                    "IO Pin",
                    "Others"});

    createSubItems(createSubItem(cell_item, "Cell Blockage"),
                   {"Block",
                    "StdCell",
                    "IO Cell",
                    "Overlap",
                    "Others"});

    createSubItem(cell_item, "Cell Layout/GDS");
}

ComponentsWidget::~ComponentsWidget() {
}

void ComponentsWidget::addComponentListener(ComponentListener* listener) {
    component_listener_list_.append(listener);
}

void ComponentsWidget::createSubItems(QTreeWidgetItem* parent_item, QList<const char*>& list) {
    for (auto name : list) {
        auto sub_item = new QTreeWidgetItem(parent_item);
        parent_item->addChild(sub_item);
        sub_item->setText(kName, name);
        sub_item->setCheckState(kVisible, Qt::Unchecked);
        sub_item->setCheckState(kSelectable, Qt::Unchecked);
    }
}

void ComponentsWidget::createSubItems(QTreeWidgetItem* parent_item, QList<const char*>&& list) {
    for (auto name : list) {
        auto sub_item = new QTreeWidgetItem(parent_item);
        parent_item->addChild(sub_item);
        sub_item->setText(kName, name);
        sub_item->setCheckState(kVisible, Qt::Unchecked);
        sub_item->setCheckState(kSelectable, Qt::Unchecked);
    }
}

QTreeWidgetItem* ComponentsWidget::createSubItem(QTreeWidgetItem* parent_item, const char* name) {
    auto sub_item = new QTreeWidgetItem(parent_item);
    parent_item->addChild(sub_item);
    sub_item->setText(kName, name);
    sub_item->setCheckState(kVisible, Qt::Unchecked);
    sub_item->setCheckState(kSelectable, Qt::Unchecked);
    return sub_item;
}

QTreeWidgetItem* ComponentsWidget::createTopItem(QTreeWidget* parent_item, const char* name) {
    auto item = new QTreeWidgetItem(parent_item);
    item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsTristate | Qt::ItemIsUserCheckable);
    item->setText(kName, tr(name));
    item->setCheckState(kVisible, Qt::Unchecked);
    item->setCheckState(kSelectable, Qt::Unchecked);
    return item;
}

void ComponentsWidget::slotItemClicked(QTreeWidgetItem* item, int column) {
    switch (column) {
    case kName:
        break;
    case kColor:
        pal.show();
        break;
    case kVisible:
        for (auto list : component_listener_list_) {
            list->setComponentVisible(item->text(kName), item->checkState(column));
        }
        break;
    case kSelectable:
        break;
    default:
        break;
    }
}
} // namespace gui
} // namespace open_edi