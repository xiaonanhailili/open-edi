#ifndef EDI_GUI_COMPONENTS_WIDGET_H_
#define EDI_GUI_COMPONENTS_WIDGET_H_

#include <QAbstractTableModel>
#include <QHeaderView>
#include <QList>
#include <QMap>
#include <QTreeWidget>
#include <QWidget>

#include "components_listener.h"
#include "components_palette.h"

namespace open_edi {
namespace gui {

class ComponentsWidget : public QTreeWidget {
    Q_OBJECT
  public:
    explicit ComponentsWidget(QWidget* parent = nullptr);
    ~ComponentsWidget();

    void addComponentListener(ComponentListener* listener);

  private:
    enum {
        kName,
        kColor,
        kVisible,
        kSelectable
    };
    QList<ComponentListener*> component_listener_list_;
    ComponentsPalette         pal;
    void                      createSubItems(QTreeWidgetItem* parent_item, QList<const char*>& list);
    void                      createSubItems(QTreeWidgetItem* parent_item, QList<const char*>&& list);
    QTreeWidgetItem*          createSubItem(QTreeWidgetItem* parent_item, const char* name);
    QTreeWidgetItem*          createTopItem(QTreeWidget* parent_item, const char* name);
  private slots:

    void slotItemClicked(QTreeWidgetItem* item, int column);

  private:
};

} // namespace gui
} // namespace open_edi

#endif // LAYER_WIDGET_H
