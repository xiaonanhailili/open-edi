#include "components_palette.h"
namespace open_edi {
namespace gui {
ComponentsPalette::ComponentsPalette(QWidget* parent) : QWidget(parent) {
    color_label_ = new QLabel(this);
    color_label_->setText("Color:");
    pattern_label_ = new QLabel(this);
    pattern_label_->setText("Pattern:");

    QPushButton* push_button;
    auto         horizontalLayout1 = new QHBoxLayout();

    horizontalLayout1->addWidget(color_label_);

    QColor color_array[10] = {
      QColor("White"),
      QColor("Red"),
      QColor("Green"),
      QColor("Blue"),
      QColor("Yellow"),
      QColor("#FF00FF"),
      QColor("#00FFFF"),
      QColor("#780078"),
      QColor("#FFC0C0"),
      QColor("#FFA800")
    };

    for (int i = 0; i < 10; i++) {
        push_button = new QPushButton(this);
        push_button->setText("");
        push_button->setFixedSize(20, 20);
        auto pal = push_button->palette();
        pal.setColor(QPalette::Button, color_array[i]);  
        push_button->setPalette(pal);
        horizontalLayout1->addWidget(push_button);
    }

    auto horizontalLayout2 = new QHBoxLayout();
    horizontalLayout2->addWidget(pattern_label_);

    for (int i = 0; i < 10; i++) {
        push_button = new QPushButton(this);
        push_button->setText("");
        push_button->setFixedSize(20, 20);
        auto pal = push_button->palette();
        pal.setBrush(QPalette::Button,QBrush(QColor("Black"),Qt::VerPattern));
        push_button->setBackgroundRole(QPalette::Button);
        push_button->setPalette(pal);
        horizontalLayout2->addWidget(push_button);
    }

    auto verticalLayout = new QVBoxLayout(this);

    verticalLayout->addLayout(horizontalLayout1);
    verticalLayout->addLayout(horizontalLayout2);

    setLayout(verticalLayout);
}

ComponentsPalette::~ComponentsPalette() {
}
} // namespace gui
} // namespace open_edi