#ifndef LAYER_LIST_H
#define LAYER_LIST_H

#include <QList>
#include "layer.h"

namespace open_edi {
namespace gui {

class LayerListener;

class LayerList {
  public:
    LayerList();
    virtual ~LayerList();

    int count() const {
        return layers_.count();
    }

    Layer* at(int i) {
        return layers_.at(i);
    }

    QList<Layer*>::Iterator begin();
    QList<Layer*>::Iterator end();

  private:
    QList<Layer*>         layers_;
    QList<LayerListener*> listeners_;
    Layer*                active_layer_;
};

} // namespace gui
} // namespace open_edi

#endif // LAYER_LIST_H
