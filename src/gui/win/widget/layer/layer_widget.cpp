
#include "layer_widget.h"

namespace open_edi {
namespace gui {

LayerWidget::LayerWidget(QWidget* parent) : QTreeWidget(parent) {

    setColumnCount(kSelectable + 1);

    auto treewidgetitem = headerItem();
    treewidgetitem->setText(kName, "Name");
    treewidgetitem->setText(kColor, "");
    treewidgetitem->setText(kVisible, "V");
    treewidgetitem->setText(kSelectable, "S");

    // setColumnWidth(kName, 50);
    header()->setSectionResizeMode(kName, QHeaderView::Stretch);
    setColumnWidth(kColor, 20);
    header()->setSectionResizeMode(kColor, QHeaderView::Fixed);
    setColumnWidth(kVisible, 20);
    header()->setSectionResizeMode(kVisible, QHeaderView::Fixed);
    setColumnWidth(kSelectable, 20);
    header()->setSectionResizeMode(kSelectable, QHeaderView::Fixed);
    header()->setStretchLastSection(false);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    treewidgetitem = new QTreeWidgetItem(this);
    treewidgetitem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsTristate | Qt::ItemIsUserCheckable);
    treewidgetitem->setCheckState(kVisible, Qt::Unchecked);
    treewidgetitem->setCheckState(kSelectable, Qt::Unchecked);
    treewidgetitem->setBackground(kColor, QBrush(QColor("Red")));

    treewidgetitem = new QTreeWidgetItem(this);
    treewidgetitem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsTristate | Qt::ItemIsUserCheckable);
    treewidgetitem->setCheckState(kVisible, Qt::Unchecked);
    treewidgetitem->setCheckState(kSelectable, Qt::Unchecked);
    treewidgetitem->setBackground(kColor, QBrush(QColor("Yellow")));

    connect(this, &LayerWidget::itemClicked, this, &LayerWidget::slotItemClicked);
}

LayerWidget::~LayerWidget() {
}

void LayerWidget::addLayerListener(LayerListener* listener) {
    layer_listener_list_.append(listener);
}

void LayerWidget::slotItemClicked(QTreeWidgetItem* item, int column) {

    switch (column) {
    case kName:
        break;
    case kColor:
        pal.show();
        break;
    case kVisible:
        // for (auto list : layer_listener_list_) {
        //     list->setLayerVisible(item->text(column),item->checkState(column));
        // }
        break;
    case kSelectable:
        break;
    default:
        break;
    }
}
} // namespace gui
} // namespace open_edi