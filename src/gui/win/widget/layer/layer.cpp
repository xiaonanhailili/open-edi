#include "layer.h"

namespace open_edi {
namespace gui {

Layer::Layer(const QString& name)
  : name_(name), visible(true) {
}

} // namespace gui
} // namespace open_edi
