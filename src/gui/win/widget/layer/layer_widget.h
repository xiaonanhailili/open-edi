#ifndef LAYER_WIDGET_H
#define LAYER_WIDGET_H

#include <QAbstractTableModel>
#include <QHeaderView>
#include <QList>
#include <QListWidget>
#include <QMap>
#include <QPushButton>
#include <QStackedWidget>
#include <QTableView>
#include <QTreeWidget>
#include <QWidget>

#include "layer_listener.h"
#include "layer_palette.h"

namespace open_edi {
namespace gui {

class LayerWidget : public QTreeWidget {
    Q_OBJECT
  public:
    explicit LayerWidget(QWidget* parent = nullptr);
    ~LayerWidget();

    void addLayerListener(LayerListener* listener);

  private:
    enum {
        kName,
        kColor,
        kVisible,
        kSelectable
    };
    QTreeWidgetItem*      widgetitem_;
    QList<LayerListener*> layer_listener_list_;
    LayerPalette          pal;
  private slots:

    void slotItemClicked(QTreeWidgetItem* item, int column);
};

} // namespace gui
} // namespace open_edi

#endif // LAYER_WIDGET_H
