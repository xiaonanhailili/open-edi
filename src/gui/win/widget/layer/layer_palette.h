#ifndef EDI_GUI_LAYER_PALETTE_H_
#define EDI_GUI_LAYER_PALETTE_H_

#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QHBoxLayout>

namespace open_edi {
namespace gui {

class LayerPalette : public QWidget {
    Q_OBJECT
  public:
    LayerPalette(QWidget* parent = nullptr);
    ~LayerPalette();

  private:
    QLabel* color_label_;
    QLabel* pattern_label_;
};

} // namespace gui
} // namespace open_edi

#endif