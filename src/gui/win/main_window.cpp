#include "main_window.h"
#include "common/action_group_manager.h"
#include "common/action_handler.h"
#include "common/action_producer.h"
#include "common/dialog_manager.h"
#include "common/docks_manager.h"
#include "dialog/import_dlg.h"
#include "util/util.h"
#include "widget/mdi_window.h"
#include "widget/ribbon/ribbon.h"
#include "widget/ribbon/ribbon_file_menu.h"
#include "widget/ribbon/ribbon_group.h"
#include "widget/ribbon/ribbon_page.h"
#include "widget/ribbon/ribbon_title_bar.h"

#include <QDockWidget>

namespace open_edi {
namespace gui {

MainWindow* MainWindow::instance_ = nullptr;

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent) {
    setObjectName("MainWindow");
    setAcceptDrops(true);

    init();
    addActions();
    createCentralWindow();
    statusBar();
}

MainWindow::~MainWindow() {
}

void MainWindow::setTclInterp(Tcl_Interp* interp) {
    interp_ = interp;
    DIALOG_MANAGER->setTclInterp(interp_);
    connect(DIALOG_MANAGER, SIGNAL(finishReadData()), graphics_view_, SLOT(slotReadData()));
}

void MainWindow::closeEvent(QCloseEvent* e) {
    hide();
    e->ignore();
}

void MainWindow::init() {
    ribbon_ = new RibbonMenuBar(this);
    setMenuBar(ribbon_);
    ribbon_->setFixedHeight(150);

    action_handler_ = new ActionHandler(this);
    action_handler_->setView(graphics_view_);
    action_manager_          = new ActionGroupManager(this);
    ActionProducer* producer = new ActionProducer(this, action_handler_);
    producer->fillActionContainer(action_map_, action_manager_);
    producer->addOtherAction(action_map_, action_manager_);

    DocksManager* docks = new DocksManager(this);
    docks->createDockWidgets();

    QMenu* menu = new QMenu;
    foreach (auto dock, findChildren<QDockWidget*>()) {
        menu->addAction(dock->toggleViewAction());
    }
    action_map_["Widgets"]->setMenu(menu);
}

void MainWindow::addActions() {
    QString res_path = QString::fromStdString(open_edi::util::getInstallPath()) + "/share/etc/res/tool/";

    RibbonFileMenu* menu = qobject_cast<RibbonFileMenu*>(ribbon_->getFileButton()->menu());
    menu->addFileAction(action_map_["ImportDesign"], Qt::ToolButtonTextBesideIcon);

    RibbonPage*  page  = ribbon_->addPage(tr("Common"));
    RibbonGroup* group = page->addGroup(tr("View&Edit"));
    group->addAction(action_map_["ZoomOut"], 0, 0, 1, 1);
    group->addAction(action_map_["ZoomIn"], 0, 1, 1, 1);
    group->addAction(action_map_["Undo"], 1, 0, 1, 1);
    group->addAction(action_map_["Redo"], 1, 1, 1, 1);

    group = page->addGroup(tr("Window"));
    group->addAction(action_map_["Widgets"], Qt::ToolButtonTextBesideIcon);
}

void MainWindow::createCentralWindow() {

    setCentralWidget(graphics_view_);
}

} // namespace gui
} // namespace open_edi
