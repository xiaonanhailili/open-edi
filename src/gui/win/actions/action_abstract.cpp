#include "action_abstract.h"

namespace open_edi {
namespace gui {

ActionAbstract::ActionAbstract(QString name, GraphicsView& view, QObject* parent)
  : QObject(parent), view_(&view), action_name_(name) {
}

} // namespace gui
} // namespace open_edi
